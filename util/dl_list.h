#ifndef DL_LIST_HEADER
#define DL_LIST_HEADER

struct node {
    void* data;
    struct node* next;
    struct node* prev;
};

#endif