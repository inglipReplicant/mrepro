#ifndef STR_UTIL
#define STR_UTIL

#include <string.h>
#include "list.h"

struct node* string_to_list(char* str, char delim);

#endif