#ifndef LIST_HEADER
#define LIST_HEADER

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
//#include <malloc.h>   micem jer je deprecated izgleda? barem na BSD-u

struct node {
    void* data;
    struct node* next;
};

/*
 * Inserts a value to the list.
 * 
 * Usage example:
 * insert(data, head);
 * 
 * alt:
 * struct node* list;
 * insert(data, &list);
 */
int insert(void* data, struct node** head);

/*
 * Gets a value at a given index.
 * 
 * Usage example:
 * struct str* val = (str*)get(head, index);
 */
void* get(struct node** head, int index);

/*
 * Deallocates the list.
 */
void clear(struct node** head);

#endif