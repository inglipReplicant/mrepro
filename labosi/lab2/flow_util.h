#ifndef FD_UTIL
#define FD_UTIL

#include <fcntl.h>

#define F_NOBLOCK  0
#define F_BLOCK    1

/**
 * Toggles O_NONBLOCK in a file descriptor.
 * 
 * @param fd - File descriptor
 * @param blocking - 0 for non-blocking, 1 for blocking
 * 
 * @return 0: success, 1: failure
 **/
int fd_toggle_blocking(int fd, int blocking);

#endif