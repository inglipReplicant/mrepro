#include "list.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int insert(void* data, struct node** head)
{
    struct node* node = (struct node*)malloc((unsigned)sizeof(struct node));
    node->data = data;
    node->next = NULL;

    if(*head) {
        node->next = *head;
    }
    *head = node;
    return 0; 
}


void* get(struct node** head, int index)
{
    struct node* ptr = *head;

    for (int i = 0; ptr; i++) {
        if (i == index) {
            return ptr->data;
        } else {
            ptr = ptr->next;
        }
    }
    return NULL;
}


void clear(struct node** head)
{
    struct node* ptr = *head;
    *head = NULL;

    struct node* next;
    while (ptr) {
        next = ptr->next;
        free(ptr);
        ptr = next;
    }
    return;
}
