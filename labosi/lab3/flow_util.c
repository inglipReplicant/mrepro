#include "flow_util.h"

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <signal.h> // You may need this.
#include <errno.h> // You will need this.


int fd_toggle_blocking(int fd, int blocking) {
    /* Save the current flags */
    int flags = fcntl(fd, F_GETFL, 0);
    if (flags == -1)
        return 0;

    if (blocking)
        flags &= ~O_NONBLOCK;
    else
        flags |= O_NONBLOCK;
    return fcntl(fd, F_SETFL, flags) != -1;
}

void daemonize()
{
    int pid, sid;
 
    // Forking and killing father.
    pid = fork();
    if(pid < 0){
        perror("fork"); // Fork failed.
        exit(1);
    }
    if(pid > 0) exit(0); // Father process.
 
    // Setting umask to zero.
    umask(0);
 
    // Getting brand new session and PID.
    sid = setsid();
    if(sid < 0){
        perror("setsid"); // setsid() failed.
        exit(3);
    }
 
    // Changing current directory.
    if(chdir("/") < 0){ // Set appropriately.
        perror("chdir"); // chdir() failed.
        exit(4);
    }
 
    // Closing standard descriptors...
    close(STDIN_FILENO); // No more listening.
    close(STDOUT_FILENO); // No more talking.
    close(STDERR_FILENO); // No more failing (or so...)
}


void sigchild_handler(int s)
{
    int saved_errno = errno;

    while(waitpid(-1, NULL, WNOHANG));

    errno = saved_errno;
}