#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <err.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#define DEFAULT_PORT 1234
#define BUFFER_SIZE 512

int main(int argc, char **argv)
{
    int port, opt, sock, r;
    int p_found = 0, l_found = 0;
    //char *port_array;
    char payload_array[BUFFER_SIZE] = "\0";
    char buffer[BUFFER_SIZE];
    char expected_message[] = "HELLO\n";      //dodat cu \n samo ako to botovi budu slali
    //char test_payload[] = "Vracam ti odgovor!\n";

    int optval;     //za ono debugiranje
    int msg_bytesize;
    unsigned int clientlen;

    struct sockaddr_in serveraddr;
    struct sockaddr_in clientaddr;
    struct hostent* host;
    char *hostaddrdecimal;

    if (argc > 5) {
        errx(1, "Usage: %s [-l port] [-p payload]", argv[0]);
    }

    while ((opt = getopt(argc, argv, "l:p:")) != -1) {
        switch (opt) {
        case 'l':
            l_found = 1;
            port = atoi(optarg);
            break;
        case 'p':
            p_found = 1;
            strncpy(payload_array, optarg, strlen(optarg));
            payload_array[strlen(optarg)] = '\0';
            break;
        default:
            fprintf(stderr, "Usage: %s [-l port] [-p payload]\n",
                argv[0]);
            exit(1);
        }
    }

    port = l_found ? port : DEFAULT_PORT;
    if (p_found == 0) {
        memset(&payload_array, 0, strlen(payload_array));
    }
    if (port <= 0 || port > 65535) {
        errx(1, "Invalid port parameter. (Must be within valid port range.)\n");
    }

    //kontrolni ispis
    printf("Port: %d\nPayload: %s\n", port, payload_array);

    if ((sock = socket(PF_INET, SOCK_DGRAM, 0)) < 0) {
        errx(1, "Error opening socket.\n");
    }

    //nasao na netu, eliminira ERROR on binding: Addr alr in use error
    optval = 1;
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (const void *)&optval , sizeof(int));

    //izgradi vlastitu adresu
    memset(&serveraddr, 0, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons((unsigned short)port);
    serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);

    if ((r = bind(sock, (struct sockaddr *)&serveraddr, sizeof(serveraddr))) < 0) {
        errx(1, "Error while binding to socket.\n");
    }

    clientlen = sizeof(clientaddr);
    while(1) {

        //pocisti buffer da moze odgovoriti
        memset(buffer, 0, strlen(buffer));

        //pokupi datagram
        msg_bytesize = recvfrom(sock, buffer, BUFFER_SIZE, 0,
            (struct sockaddr *)&clientaddr, &clientlen);
        if (msg_bytesize < 0) {
            errx(1, "Error while receiving message.\n");
        }

        printf("Adresa je? %s\n", inet_ntoa(clientaddr.sin_addr));
        host = gethostbyaddr((const char *)(&clientaddr.sin_addr.s_addr), 
            sizeof(clientaddr.sin_addr.s_addr), AF_INET);
        if (host == NULL) {
            errx(1, "ERROR in gethostbyaddr.\n");
        }

        hostaddrdecimal = inet_ntoa(clientaddr.sin_addr);
        if (hostaddrdecimal == NULL) {
            errx(1, "Error while parsing address from network to host\n");
        }
        printf("Server received datagram from %s (%s)\n",
            host->h_name, hostaddrdecimal);
        printf("Server received %d/%d bytes: %s\n",
            (int)strlen(buffer), msg_bytesize, buffer); 

        //todo: napisati nesto sto ce sprijeciti buffer overflow
        //mislim da je ok ovako?

        //gledamo je li HELLO tu ili nije
        if (strcmp(expected_message, buffer) == 0) {
            r = sendto(sock, (const char *)payload_array, sizeof(payload_array),
                0, (struct sockaddr *)&clientaddr, sizeof(clientaddr));
            if (r == -1) {
                errx(1, "Error while responding to the client.\n");
            }
        }
    }
    return 0;
}
