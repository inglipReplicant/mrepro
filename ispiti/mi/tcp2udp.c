#include "wrappers.h"
#include "zadheader.h"

int main(int argc, char** argv)
{
	int port, port_set = 0, opt;
	int sockfd;
	struct addrinfo hints, *res;
	char* port_string = DEFAULT_PORT;
	//socklen_t client_size;
	
	while ((opt = getopt(argc, argv, "p:")) != -1)
	{
		switch(opt) {
			case 'p':
				port_set = 1;
				port = atoi(optarg);
				port_string = optarg;
				if (port < 1024 || port > 65535) {
					fprintf(stderr, "Invalid port range.\n");
					exit(EXIT_F);
				}
				break;
			default:
				errx(EXIT_F, "Usage: ./tcp2udp [-p tcp_port]");
		}
	}
	if (argv[optind] != NULL) {
		errx(EXIT_F, "Too many arguments.");
	}

	if (port_set) {
		//printf("Dobio sam port: %s\n", port_string);
	} else {
		//debug_print("Postavljam defaultni port.\n");
		port_string = (char*)malloc(sizeof(DEFAULT_PORT));
		port_string = strncpy(port_string, DEFAULT_PORT, sizeof(DEFAULT_PORT));
	}

	memset(&hints, 0, sizeof(hints));
	set_hints(&hints, AF_INET, SOCK_STREAM);
	hints.ai_flags = AI_PASSIVE;

	get_addr_info(NULL, port_string, &hints, &res);
	sockfd = open_socket(AF_INET, SOCK_STREAM, 0);
	reuse_dead_socket(sockfd);
	bind_socket(sockfd, res->ai_addr, res->ai_addrlen);
	sock_listen(sockfd, BACKLOG);

	//printf("Uspjesno sam bindao port %s na socket %d.\n",
	//	port_string, sockfd);
	
	return start_receiving(sockfd);
}

int start_receiving(int sockfd)
{
	int newsock;
	struct sockaddr_in client;
	socklen_t client_size;
	ssize_t recv_size;
	char message[BUFFER_SIZE] = {0};
	
	client_size = sizeof(client);
	
	while(1)
	{	
		newsock = accept_request(sockfd, (struct sockaddr*)(&client),
			&client_size);
		print_connection((struct sockaddr*)(&client), MODE_ON);

		while((recv_size = wrap_recv(newsock, message, sizeof(message), 0)) > 0)
		{
			//printf("Primio sam poruku: %s", message);

			if (strncmp(message, QUIT, sizeof(QUIT)) == 0) {	
				print_connection((struct sockaddr*)(&client), MODE_OFF);
				return 0;
			} else if (strncmp(message, STAT, sizeof(STAT)) == 0) {
				wrap_send(newsock, OK, sizeof(OK), 0);
			} else {
				send_messages(sockfd, message);
			}
			memset(message, 0, sizeof(message));
		}	
		print_connection((struct sockaddr*)(&client), MODE_OFF);
	}
}

void send_messages(int sockfd, char* message)
{
	int ind = 5;
	struct pair_node* head;
	struct pair_node* last;	

	char msg[BUFFER_SIZE] = {0};
	strcpy(msg, message);

	while(msg[ind] != '#') {
		struct pair_node temp;
		struct pair p;
		
		int ctr = 0;

		if (ind == 5) {
			head = &temp;	//tek smo poceli, ovo je prvi par u poruci
			last = head;
		} else {
			last->next = &temp;
		}

		memset(&temp, 0, sizeof(temp));
		memset(&p, 0, sizeof(p));	

		temp.pair = &p;

		while(msg[ind] != ':') {
			p.ip[ctr++] = msg[ind++];
		}
	
		ctr = 0;
		ind++;
		while(msg[ind] != ';') {
			if (msg[ind] != '#') {
				p.port[ctr++] = msg[ind++];
			} else {
				printf("%s\n", p.port);
				break;
			}
		}

		//dodati podrsku za listu kasnije ako stignem
	}
	last = head;
	while(last != 0) {
		sendto(sockfd, message, sizeof(message) - 1, 0,
			last->pair->ip, sizeof(last->pair->ip));
		last = last->next;
	}
}

void print_connection(struct sockaddr* peer, int mode)
{
	void *addr;
	uint16_t temp_port;
	uint16_t port;
	char ip_string[INET6_ADDRSTRLEN];
	char* onoff;

	struct sockaddr_in* saddr = (struct sockaddr_in*) peer;
	addr = &(saddr->sin_addr);
	temp_port = saddr->sin_port;

	port = ntohs(port);
	inet_ntop(AF_INET, addr, ip_string, sizeof(ip_string));

	if (mode == MODE_ON) {
		onoff = CONN_ON;
	} else if (mode == MODE_OFF) {
		onoff = CONN_OFF;
	} else errx(EXIT_F, "Use either 0 (MODE_ON) or 1 (MODE_OFF) for mode.");

	printf("%s:%s:%d\n",onoff, ip_string, port);
	return;
}
