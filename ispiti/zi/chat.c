#include "zad_header.h"
#include "error_util.h"
#include "net_util.h"
#include "net_abst.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <err.h>
#include <errno.h>

#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define OPT_LIST "t:u:"
#define MAX_ARGS 4
#define MIN_ARGS 0

void reset_clients(int* clients, int* index);

void send_server_to_tcp(char* service, char* msg, int* clients);

void send_to_tcp_clients(char* service, char* ip,
	char* port, char* msg, int* clients);
	
void send_tcp_to_others(int index, char* ip, 
	char* port, char* msg, int* tcp_clients);

int main(int argc, char** argv) {
    int opt;
    char udp_port[10] = DEFAULT_PORT;
	char tcp_port[10] = DEFAULT_PORT;
	char buf_udp[BUFFER_SIZE], buf_tcp[BUFFER_SIZE], buf_stdin[BUFFER_SIZE];
	
	memset(buf_tcp, '\0', BUFFER_SIZE);
	
	struct sockaddr_in tcp_serv, udp_serv;
	int tcp_clients[BACKLOG];
	int client_index = 0;

	unsigned int ndfs = 3;
	struct pollfd ufds[ndfs];
	int rv = 0;
	int sock_udp, sock_tcp, sock_stdin;
	ssize_t ret_read;

    if (argc < MIN_ARGS + 1 || argc > MAX_ARGS + 1) {
        fprintf(stderr, "Usage: %s [-t tcp_port] [-u udp_port] \n",
            argv[0]);
        exit(EXIT_F);
    }

    while ((opt = getopt(argc, argv, OPT_LIST)) != -1)
    {
        switch(opt) {
            case 't':
                //printf("Koristi se TCP port %s\n", optarg);
				strncpy(tcp_port, optarg, (size_t)10);
                break;
            case 'u':
                //printf("Koristi se UDP port %s\n", optarg);
				strncpy(udp_port, optarg, (size_t)10);
                break;
            default:
                fprintf(stderr, "Usage: %s [-t tcp_port] [-u udp_port]\n",
                    argv[0]);
                exit(EXIT_F);
        }
    }

    //if no optional arguments are supported
    if (argv[optind] != NULL) {
        fprintf(stderr, "Usage: %s [-t tcp_port] [-u udp_port]\n",
            argv[0]);
        exit(EXIT_F);
    }

	for (int i = 0; i < BACKLOG; i++) {
		tcp_clients[i] = -1;
	}

   	init_tcp_server_v4(&tcp_serv, tcp_port);
	init_udp_server_v4(&udp_serv, udp_port);

	sock_udp = open_socket(PF_INET, SOCK_DGRAM, 0);
	sock_stdin = 0;

	reuse_dead_socket(sock_udp);

	bind_socket(sock_udp, (struct sockaddr*)(&udp_serv), sizeof(struct sockaddr_in));

	ufds[0].fd = sock_stdin;
	ufds[0].events = POLLIN;

	ufds[2].fd = sock_udp;
	ufds[2].events = POLLIN;

	while(1)
	{
		rv = poll(ufds, ndfs, 4000);

		if (rv == -1) {
			wrap_errx("Server main", EXIT_F);
		} else if (rv == 0) {
			//printf("Timeout occurred.\n");
		} else {
			//stdin
			if (ufds[0].revents & POLLIN) {
				ret_read = wrap_read(sock_stdin, buf_stdin, BUFFER_SIZE - 1);
				buf_stdin[ret_read - 1] = '\0';
				
				if (ret_read <= 0) {
					errx(EXIT_F, "Greska kod citanja sa stdinputa");
				}
				
				if (strncmp(buf_stdin, CMD_ON, strlen(CMD_ON) - 1) == 0)
				{
					sock_tcp = open_socket(PF_INET, SOCK_STREAM, 0);
					reuse_dead_socket(sock_tcp);
					bind_socket(sock_tcp, (struct sockaddr*)(&tcp_serv), sizeof(struct sockaddr_in));
					sock_listen(sock_tcp, BACKLOG);
					
					ufds[1].fd = sock_tcp;
					ufds[1].events = POLLIN;
				}
				else if (strncmp(buf_stdin, CMD_RESET, strlen(CMD_RESET) - 1) == 0)
				{
					reset_clients(tcp_clients, &client_index);
				}
				else if (strncmp(buf_stdin, CMD_OFF, strlen(CMD_OFF) - 1) == 0)
				{
					wrap_close(sock_tcp);
				}
				else
				{
					send_server_to_tcp("Server", buf_stdin, tcp_clients);
				}
			}
			
			if (ufds[1].revents & POLLIN) {
				//tcp
				struct sockaddr_in their_addr;
                socklen_t addrlen = sizeof(their_addr);
				char ip_addr[INET_ADDRSTRLEN];
				char port[20];
                
                tcp_clients[client_index] = accept_request(sock_tcp, 
                        (struct sockaddr*)(&their_addr), &addrlen);
                
                if (tcp_clients[client_index] < 0) {
                    fprintf(stderr, "Error while accepting a TCP request\n");
                }
				get_name_info((struct sockaddr*)(&their_addr), addrlen,
						ip_addr, sizeof(ip_addr), port, sizeof(port),
						NI_NUMERICHOST | NI_NUMERICSERV);
				
				fprintf(stderr, "Spojio se:%s:%s\n", ip_addr, port);
				
				ret_read = wrap_recv(tcp_clients[client_index], buf_tcp, BUFFER_SIZE, 0);
				buf_tcp[ret_read - 1] = '\0';
				
				if (strlen(buf_tcp) != 0) {
					send_tcp_to_others(client_index, ip_addr, 
						port, buf_tcp, tcp_clients);
					memset(buf_tcp, '\0', BUFFER_SIZE);
				}
				
				while (tcp_clients[client_index] != -1) {
                    client_index = (client_index + 1) % BACKLOG;
                }
			}
			
			if (ufds[2].revents & POLLIN) {
				//udp
				struct sockaddr_storage their_addr;
				socklen_t addrlen = sizeof their_addr;
				ssize_t numbytes;
				
				char ip_addr[INET_ADDRSTRLEN];
				char port[20];
				
				numbytes = wrap_recvfrom(sock_udp, buf_udp, BUFFER_SIZE, 0,
						(struct sockaddr*)(&their_addr), &addrlen);
				buf_udp[numbytes - 1] = '\0';

				get_name_info((struct sockaddr*)(&their_addr), addrlen,
						ip_addr, sizeof(ip_addr), port, sizeof(port),
						NI_NUMERICHOST | NI_NUMERICSERV);
				
				if (ret_read <= 0) {
					errx(EXIT_F, "Greska kod citanja sa UDP-a");
				}
				
				if (strncmp(buf_udp, CMD_ON, strlen(CMD_ON) - 1) == 0) {
					sock_tcp = open_socket(PF_INET, SOCK_STREAM, 0);
					reuse_dead_socket(sock_tcp);
					bind_socket(sock_tcp, (struct sockaddr*)(&tcp_serv), sizeof(struct sockaddr_in));
					sock_listen(sock_tcp, BACKLOG);
					
					ufds[1].fd = sock_tcp;
					ufds[1].events = POLLIN;
				}
				else if (strncmp(buf_udp, CMD_RESET, strlen(CMD_RESET) - 1) == 0)
				{
					reset_clients(tcp_clients, &client_index);
				}
				else if (strncmp(buf_udp, CMD_OFF, strlen(CMD_OFF) - 1) == 0)
				{
					wrap_close(sock_tcp);
				}
				else
				{
					fprintf(stderr, "UDP:%s:%s\n", ip_addr, port);
					send_to_tcp_clients("UDP", ip_addr, port, buf_udp, tcp_clients);
				}
			}
		}
	}
    return 0;
}

void reset_clients(int* clients, int* index);
{
	//printf("Zatvaram klijente.\n");
	for (int i = 0; i < BACKLOG; i++) {
		if (clients[i] != -1) {
			wrap_shutdown(clients[i], SHUT_RDWR);
			wrap_close(clients[i]);
			clients[i] = -1;
		}
	}
	*index = 0;
}

void send_to_tcp_clients(char* service, char* ip,
	char* port, char* msg, int* clients)
{
	char message[BUFFER_SIZE];
	memset(message, '\0', BUFFER_SIZE);
	sprintf(message, "%s:%s:%s:%s\n", service, ip, port, msg);

	for (int i = 0; i < BACKLOG; i++) {
		if (clients[i] != -1) {
			wrap_write(clients[i], message, strlen(message));
		}
	}
}


void send_server_to_tcp(char* service, char* msg, int* clients)
{
	char message[BUFFER_SIZE];
	memset(message, '\0', BUFFER_SIZE);
	sprintf(message, "%s:%s\n", service, msg);

	for (int i = 0; i < BACKLOG; i++) {
		if (clients[i] != -1) {
			wrap_write(clients[i], message, strlen(message));
		}
	}
}


void send_tcp_to_others(int index, char* ip, 
		char* port, char* msg, int* tcp_clients)
{
	char message[BUFFER_SIZE];
	memset(message, '\0', BUFFER_SIZE);
	sprintf(message, "TCP:%s:%s:%s\n", ip, port, msg);
	
	fprintf(stderr, "%s", message);
	for (int i = 0; i < BACKLOG; i++) {
		if (tcp_clients[i] > 0 && i != index) {
			wrap_write(tcp_clients[i], message, strlen(message));
		}
	}
}
