#ifndef ZADHDR_INCLUDED
#define ZADHDR_INCLUDED

#define BUFFER_SIZE 1024
#define DEFAULT_PORT "1234"
#define DEFAULT_HOST "127.0.0.1"

#define ERR_READ "Insufficient reading permissions for given file."
#define ERR_MISS "No such file."
#define ERR_GNRL "Oops. Something went wrong!"

struct request {
    int offset;
    char filename[BUFFER_SIZE];
};

#endif
