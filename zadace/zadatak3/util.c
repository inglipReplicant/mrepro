#include "wrappers.h"


void debug_print(char* text) 
{
    printf("%s", text);
}


void wrap_errx(char* caller, int errind)
{
	errx(EXIT_F, "%s: %d - %s", caller, errind, strerror(errind));
}


void set_hints(struct addrinfo* hints, int family, int socktype)
{
	hints->ai_family = family;
	hints->ai_socktype = socktype;
}


void get_addr_info(const char* server, const char* service,
		const struct addrinfo* hints, struct addrinfo** res)
{
	int status;
	if ((status = getaddrinfo(server, service, hints, res)) != 0) {
		errx(EXIT_F, "getaddrinfo: %s\n", gai_strerror(status));
	}
}


void get_name_info(const struct sockaddr* addr, socklen_t addrlen,
		char* host, socklen_t hostlen, char* serv, socklen_t servlen, int flags)
{
	int r;
	if ((r = getnameinfo(addr, addrlen, host, hostlen, serv, servlen, flags)) != 0) {
		fprintf(stderr, "%s", gai_strerror(r));
		wrap_errx("getnameinfo", errno);
	}
}


int open_socket(int family, int type, int protocol)
{
	int s;
	if ((s = socket(family, type, protocol)) < 0) {
		wrap_errx("socket", errno);
	}
	return s;
}

void reuse_dead_socket(int socket)
{
	int yes = 1;
	/* Should I do this? */
	if (setsockopt(socket, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) != 0) {
		wrap_errx("setsockopt", errno);
	}	
}


void bind_socket(int sockfd, const struct sockaddr* addr, 
		socklen_t addrlen)
{
	if (bind(sockfd, addr, addrlen) != 0) {
		wrap_errx("bind", errno);
	}	
}


void sock_listen(int socket, int backlog)
{
	if (listen(socket, backlog) != 0) {
		wrap_errx("listen", errno);
	}
}
// mozda prebacim u posebni .c file radi citljivosti
// connection-based je pa poseban file za TCP


int accept_request(int sockfd, struct sockaddr* addr,
		socklen_t* addrlen)
{
	int s;
	if ((s = accept(sockfd, addr, addrlen)) == -1) {
		wrap_errx("accept", errno);
	}
	return s;
}
// takoder mozda prebacim u drugi .c file


void sock_shutdown(int sockfd, int how)
{
	if (shutdown(sockfd, how) == -1) {
		wrap_errx("shutdown", errno);
	}
}


void wrap_close(int fd)
{
	if (close(fd) != 0) {
		wrap_errx("close", errno);
	}
}


void sock_connect(int sockfd, 
		const struct sockaddr* addr, socklen_t addrlen)
{
	if (connect(sockfd, addr, addrlen) != 0) {
		wrap_errx("connect", errno);
	}
}


int wrap_read(int fd, char* buf, int max)
{
	int n;
	if ((n = read(fd, buf, max)) < 0) {
		wrap_errx("read", errno);
	}
	return n;
}


int wrap_write(int fd, char* buf, int num)
{
	int n;
	if ((n = write(fd, buf, num)) < 0) {
		wrap_errx("write", errno);
	}
	return n;
}


ssize_t wrap_send(int sockfd, const void* msg,
		size_t msglen, int flags)
{
	ssize_t sent;
	if ((sent = send(sockfd, msg, msglen, flags)) == -1) {
		wrap_errx("send", errno);
	}
	return sent;
}


ssize_t wrap_recv(int sockfd, void* buf, size_t len, int flags)
{
	ssize_t res;
	if ((res = recv(sockfd, buf, len, flags)) == -1) {
		wrap_errx("recv", errno);
	}
	return res;
}


ssize_t wrap_sendto(int sockfd, const void *msg, size_t len,
		int flags, const struct sockaddr* dest_addr, socklen_t tolen)
{
	ssize_t res;
	if ((res = sendto(sockfd, msg, len, flags, dest_addr, tolen)) == -1) {
		wrap_errx("sendto", errno);
	}
	return res;
}


ssize_t wrap_recvfrom(int sockfd, void* buf, size_t len,
		int flags, struct sockaddr* from, socklen_t* fromlen)
{
	ssize_t res;
	if ((res = recvfrom(sockfd, buf, len, flags, from, fromlen)) == -1) {
		wrap_errx("recvfrom", errno);
	}
	return res;
}

/* =====================
 * Functions for getopt
 ===================== */
void check_empty_params(int optind, char** argv)
{
	if (argv[optind] == NULL) {
		errx(EXIT_F, "Missing required arguments.");
	}
}

/* ==============================
 * Functions for file operations
=============================== */

int file_open(const char* pathname, int flags)
{
	int res;
	if ((res = open(pathname, flags)) < 0) {
		wrap_errx("open", errno);
	}
	return res;
}


FILE* file_fopen(const char* pathname, const char* mode)
{
	FILE* res;
	if ((res = fopen(pathname, mode)) == NULL) {
		wrap_errx("fopen", errno);
	}
	return res;
}


void file_fclose(FILE* file)
{
	if (fclose(file) != 0) {
		wrap_errx("fclose", errno);
	}
}


size_t file_fread(void* ptr, size_t size,
		size_t nmemb, FILE* stream)
{
	size_t chunk;
	if ((chunk = fread(ptr, size, nmemb, stream)) != nmemb) {
		if (ferror(stream)) {
			errx(EXIT_F, "Error while reading the file.");
		}
	}
	return chunk;
}


size_t file_fwrite(const void* ptr, size_t size,
		size_t nmemb, FILE* stream)
{
	size_t chunk;
	if ((chunk = fwrite(ptr, size, nmemb, stream)) != nmemb) {
		if (ferror(stream)) {
			errx(EXIT_F, "Error while writing to the file.");
		}
	}
	return chunk;
}


int file_access(const char* path, int amode)
{
	int result;
	if ((result = access(path, amode)) == -1) {
		wrap_errx("access", errno);
	}
	return result;
}


int file_exists(const char* path)
{
	if (access(path, F_OK) != -1) {
		return 1;
	}
	return 0;
}


int file_fseek(FILE* stream, long offset, int whence)
{
	int res;
	if ((res = fseek(stream, offset, whence)) == -1) {
		wrap_errx("fseek", errno);
	}
	return res;
}

long file_ftell(FILE* stream) {
	long res;
	if ((res = ftell(stream)) == -1) {
		wrap_errx("ftell", errno);
	}
	return res;
}
