#include "wrappers.h"
#include "zadheader.h"

void file_checks(const char* filename, int cont);
int calculate_offset(FILE* file, const char* path, int cont);

int main(int argc, char** argv) 
{
	int opt, port_set = 0, serv_set = 0, opt_index = 0, paramc = 0;
	int cont = 0, port;
	char* server;
	char* port_string;
	char* filename;

	int offset, status = 0;
	FILE* file;
	ssize_t rcvd;

	int sockfd;
	struct addrinfo hints, *res;
	char buf[BUFFER_SIZE];
	struct request rqst;
 
	while ((opt = getopt(argc, argv, "s:p:c")) != -1)
	{
		switch(opt) {
			case 's':
				serv_set = 1;
				server = optarg;
				break;
			case 'p':
				port_set = 1;
				port = atoi(optarg);
				port_string = optarg;
				if (port < 1024 || port > 65535) {
						errx(EXIT_F, "Invalid port. Allowed range: [1024, 65535]\n");
				}
				break;
			case 'c':
				//debug_print("Postavljen flag za nastavak prijenosa.\n");
				cont = 1;
				break;
			default:
				fprintf(stderr, "Usage: ./tcpklijent [-s server] [-p port] \
					[-c] filename\n");
				exit(EXIT_F);
		}
	}	

	check_empty_params(optind, argv);

	for (opt_index = optind; opt_index < argc; opt_index++, paramc++) {
		if (paramc++ > 1) {
			errx(1, "Too many file arguments.\n");
		}
		filename = argv[opt_index];
	}
	//printf("Filename je %s.\n", filename);

	if (!port_set) {
		//printf("Velicina default porta je %d\n", (int)sizeof(DEFAULT_PORT));
		port_string = (char*)malloc(sizeof(DEFAULT_PORT));
		port_string = strncpy(port_string, DEFAULT_PORT, sizeof(DEFAULT_PORT));
	}

	if (!serv_set) {
		server = (char*)malloc(sizeof(DEFAULT_HOST));
		server = strncpy(server, DEFAULT_HOST, sizeof(DEFAULT_HOST));
	}
	//printf("Trazim podatke o %s:%s\n", server, port_string);

	//pripremi za slanje
	memset(buf, 0, sizeof(buf));
	memset(&hints, 0, sizeof(hints));
	memset(&rqst, 0, sizeof(rqst));
	set_hints(&hints, AF_INET, SOCK_STREAM);
	get_addr_info(server, port_string, &hints, &res);

	sockfd = open_socket(AF_INET, SOCK_STREAM, 0);
	sock_connect(sockfd, res->ai_addr, res->ai_addrlen);

	file_checks(filename, cont);		//provjeri dozvole, postojanje itd.
	file = file_fopen(filename, "a+");
	offset = calculate_offset(file, (const char*)filename, cont);	//izracunaj offset	

	strcpy(rqst.filename, filename);
	rqst.offset = offset;
	wrap_send(sockfd, &rqst, sizeof(rqst), 0);

	wrap_recv(sockfd, &status, sizeof(status), 0);
	if (status != 0) {
		errx(status, "Unable to receive file.\n");
	}

	if (cont) {
		file_fseek(file, 0, SEEK_END);
	} else {
		file_fseek(file, 0, SEEK_SET);
	}
	while ((rcvd = wrap_recv(sockfd, buf, BUFFER_SIZE, 0)) > 0) {
		//printf("Dobio sam poruku %s\n.", buf);
		file_fwrite(buf, 1, rcvd, file);
		memset(buf, 0, BUFFER_SIZE);
	}
	
	file_fclose(file);
	freeaddrinfo(res);
    return 0;
}

void file_checks(const char* filename, int cont)
{
	int exists = file_exists(filename);
	int write = 0;

	if (exists == 1) {
		write = file_access(filename, W_OK);
	}

	if (cont == 0 && exists) {
		errx(EXIT_F, "Requested file already exists.\n");
	}

	if (exists && cont == 1 && write == -1) {
		errx(EXIT_F, "Insufficient permissions: unable to write to file.\n");
	}
}

int calculate_offset(FILE* file, const char* path, int cont)
{
	
	if (cont == 0) {
		return 0;
	}

	if (cont == 1 && (file_exists(path) == 0)) {
		return 0;
	}

	file_fseek(file, 0L, SEEK_END);
	return (int)ftell(file);
}

