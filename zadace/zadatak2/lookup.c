#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <err.h>

#define FL_TCP 0
#define FL_UDP 1
#define FL_HBO 0        //host byte order
#define FL_NBO 1        //network byte order
#define FL_REV 1
#define FL_IP6 1
#define FL_HEX 1

struct flags 
{
    int f_protocol;     //0 za TCP, 1 za UDP
    int f_endianness;   //0 za host byte order, 1 za network
    int f_rev;          //reverse DNS lookup
    int f_ipv6;         //IPv6 
    int f_hex;          //print port in hex
};

struct result
{
    char ip_addr[INET6_ADDRSTRLEN];
    char canon_name[NI_MAXHOST];
    uint16_t port;
};

struct result lookup(struct flags flags, char *hostname, char *servicename);             //pass by value je dovoljan
struct result reverse_lookup(struct flags flags, char *ipaddr, char *port);
uint16_t parse_port(struct flags flags, uint16_t port);
struct result rev_lookup_in4(struct flags flags, char *ipaddr, char *port);
struct result rev_lookup_in6(struct flags flags, char *ipaddr, char *port);
void print_result(struct flags flags, struct result res);

int main (int argc, char **argv)
{
    struct flags flags = { -1, -1, 0, 0, 0 };
    int c = 0, inargs = 0, index;
    char *host, *service;
    struct result res;

    opterr = 0;     //ako ne bude 0 nakon for petlje terminiraj program

    if (argc < 3) {
        fprintf(stderr, "prog [-r] [-t|-u] [-x] [-h|-n] [-46] {hostname|IP_address} {servicename|port}\n");
        return 1;
    }

    while ((c = getopt(argc, argv, "tuhnr46x")) != -1)
        switch(c)
        {
            case 't':
                //TCP is used by default, and is mutually exclusive with the UDP arg   
                if (flags.f_protocol == FL_UDP)
                {
                    fprintf(stderr, "Cannot use both TCP and UDP in a single DNS lookup request.\n");
                    return 126;
                } else {
                    flags.f_protocol = FL_TCP;
                }
                break;
            case 'u':
                if (flags.f_protocol == FL_TCP)
                {
                    fprintf(stderr, "Cannot use both TCP and UDP in a single DNS lookup request.\n");
                    return 126;
                } else {
                    flags.f_protocol = FL_UDP;
                }
                break;
            case 'h':
                if (flags.f_endianness == FL_NBO)
                {
                    fprintf(stderr, "Cannot use both host and network byte order arguments in a single DNS lookup request.\n");
                    return 126;
                } else {
                    flags.f_endianness = FL_HBO;
                }
                break;
            case 'n':
                if (flags.f_endianness == FL_HBO)
                {
                    fprintf(stderr, "Cannot use both host and network byte order arguments in a single DNS lookup request.\n");
                    return 126;
                } else {
                    flags.f_endianness = FL_NBO;
                }
                break;
            case 'r':
                flags.f_rev = FL_REV;
                break;
            case 'x':
                flags.f_hex = FL_HEX;
                break;
            case '4':
                flags.f_ipv6 = flags.f_ipv6 == FL_IP6 ? FL_IP6 : 0;
                break;
            case '6':
                flags.f_ipv6 = FL_IP6;
                break;
            case '?':
                fprintf(stderr, "What just happened?\n");
                return 1;
            default:
                printf("Znak: %c\n", c);
                abort();
        }

    if (flags.f_protocol == -1) flags.f_protocol = FL_TCP;
    if (flags.f_endianness == -1) flags.f_endianness = FL_HBO;

    //printf("Pro: %d, End: %d, Rev: %d, Ip6: %d, Hex: %d\n", 
    //    flags.f_protocol, flags.f_endianness, flags.f_rev, flags.f_ipv6, flags.f_hex);

    for (index = optind; index < argc; index++)
    {
        inargs++;
    }
    if (inargs != 2) {
        fprintf(stderr, "Invalid number of arguments.\n");
        return 1;
    }
    service = argv[--index];
    host = argv[--index];

    //dodatno sredivanje argumenata tipa portovi i to
    if (flags.f_rev == FL_REV) {
        res = reverse_lookup(flags, host, service);
    } else {
        res = lookup(flags, host, service);
    }
    print_result(flags, res);
    return 0;
}

struct result lookup(struct flags flags, char *hostname, char *servicename)
{
    struct addrinfo hints, *result, *pom;
    int status;
    struct result final_result;

    memset(&hints, 0, sizeof hints);
    hints.ai_family = flags.f_ipv6 == FL_IP6 ? AF_INET6 : AF_INET;
    hints.ai_socktype = flags.f_protocol == FL_UDP ? SOCK_DGRAM : SOCK_STREAM;
    hints.ai_flags = AI_CANONNAME;

    if ((status = getaddrinfo(hostname, servicename, &hints, &result)) != 0)
    {
        fprintf(stderr, "Unable to retrieve address info.\n");
        exit(EXIT_FAILURE);
    }

    for (pom = result; pom != NULL; pom = pom->ai_next)
    {
        void *addr;
        uint16_t temp_port;
        uint16_t port;
        char ip_string[INET6_ADDRSTRLEN];

        //ovisi o tome je li IPv4 ili IPv6
        if (pom->ai_family == AF_INET) {
            struct sockaddr_in *saddr = (struct sockaddr_in*) pom->ai_addr;
            addr = &(saddr->sin_addr);
            temp_port = saddr->sin_port;
        } else {
            struct sockaddr_in6 *saddr = (struct sockaddr_in6*) pom->ai_addr;
            addr = &(saddr->sin6_addr);
            temp_port = saddr->sin6_port;
        }

        //ispis rezultata
        //adresa (CNAME) port
        port = parse_port(flags, temp_port);
        inet_ntop(pom->ai_family, addr, ip_string, sizeof ip_string);

        memset(&final_result, 0, sizeof(final_result));
        strcpy(final_result.ip_addr, ip_string);
        strcpy(final_result.canon_name, pom->ai_canonname);
        final_result.port = port;
    }

    freeaddrinfo(result);
    return final_result;
}

uint16_t parse_port(struct flags flags, uint16_t port)
{
    return flags.f_endianness == FL_HBO ? ntohs(port) : port;
}

void print_result(struct flags flags, struct result res)
{
    if (flags.f_rev != FL_REV) {
        if (flags.f_hex == FL_HEX) {
            printf("%s (%s) %.4x\n", res.ip_addr, res.canon_name, res.port);
        } else {
            printf("%s (%s) %d\n", res.ip_addr, res.canon_name, res.port);
        }
    } else {
        printf("%s (%s) %d\n", res.ip_addr, res.canon_name, res.port);
    }
}

struct result reverse_lookup(struct flags flags, char *ipaddr, char *port)
{
    if (flags.f_ipv6 == FL_IP6) {
        return rev_lookup_in6(flags, ipaddr, port);
    }
    return rev_lookup_in4(flags, ipaddr, port);
}

struct result rev_lookup_in6 (struct flags flags, char *ipaddr, char *port)
{
    int rev_result;
    char host[NI_MAXHOST];
    struct sockaddr_in6 addr;

    addr.sin6_family = AF_INET6;

    if (inet_pton(AF_INET6, ipaddr, &(addr.sin6_addr)) != 1)
    {
        errx(1, "%s is not a valid IPv6 address.\n", ipaddr);
    }
    rev_result = getnameinfo((struct sockaddr*) &addr, sizeof (struct sockaddr_in6),
        host, sizeof(host), NULL, 0, NI_NAMEREQD);

    if (rev_result) errx(1, "getnameinfo: %s\n", gai_strerror(rev_result));
    return lookup(flags, host, port);
}

struct result rev_lookup_in4(struct flags flags, char *ipaddr, char *port)
{
    int rev_result;
    char host[NI_MAXHOST];
    struct sockaddr_in addr;

    addr.sin_family = AF_INET;

    if (inet_pton(AF_INET, ipaddr, &(addr.sin_addr)) != 1)
    {
        errx(1, "%s is not a valid IPv4 address.\n", ipaddr);
    }

    rev_result = getnameinfo((struct sockaddr*) &addr, sizeof (struct sockaddr_in),
        host, sizeof(host), NULL, 0, NI_NAMEREQD);

    if (rev_result) errx(1, "getnameinfo: %s\n", gai_strerror(rev_result));
    return lookup(flags, host, port);
}