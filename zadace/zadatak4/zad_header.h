#ifndef ZADHDR_INCLUDED
#define ZADHDR_INCLUDED

#define TFTP_OPCODE 2
#define BUFFER_SIZE 512

#define OPCODE_RRQ  1u   //read request
#define OPCODE_WRQ  2u   //write request
#define OPCODE_DAT  3u   //data packet
#define OPCODE_ACK  4u   //acknowledgement packet
#define OPCODE_ERR  5u   //error packet

#define ERRCODE_UNDEF   0u  //undefined error
#define ERRCODE_FNOTFD  1u  //file not found
#define ERRCODE_ACCESS  2u  //access violation
#define ERRCODE_DSKFUL  3u  //disk full
#define ERRCODE_ILLOPT  4u  //illegal TFT operation
#define ERRCODE_UNKTID  5u  //unknown TID
#define ERRCODE_EXISTS  6u  //file exists
#define ERRCODE_NOUSER  7u  //no such user

#define MODE_ASCII  "netascii"  //treba biti case insensitive
#define MODE_OCTET  "octet"
#define MODE_MAIL   "mail"      //obsolete, ne koristim

#endif